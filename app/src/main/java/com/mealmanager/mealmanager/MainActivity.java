package com.mealmanager.mealmanager;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class MainActivity extends AppCompatActivity {

    PieChartView pieChartView;

    Button btnmember,btnmeal,btndeposit,btnexpanse;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pieChartView = findViewById(R.id.chart);


        List pieData = new ArrayList<>();

        pieData.add(new SliceValue(60, Color.GREEN).setLabel("Expanse"));
        pieData.add(new SliceValue(40, Color.RED).setLabel("Deposit"));


        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);

        pieChartData.setHasCenterCircle(true).setCenterText1("Meal Manager").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));

        pieChartView.setPieChartData(pieChartData);


        btnmember=(Button)findViewById(R.id.btnmember);
        btnmember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  i = new Intent(getApplicationContext(),Member.class);
                startActivity(i);
            }
        });

        btndeposit=(Button)findViewById(R.id.btndeposit);
        btndeposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),Deposit.class);
                startActivity(i);
            }
        });



    }
}
